# Hi, 👋 I'm [Mohamed Arish](https://mohamedarish.live)

- A developer eager to learn more.
- Currently learning: Rust🦀
- Currently working on: Learning rust
- Interested to do everything open-source

## Languages and technologies

[![linux](assets/linux.png "linux")](https://www.linux.org) [![html](assets/html.png "html")](https://en.wikipedia.org/wiki/HTML5) [![css](assets/css.png "css")](https://en.wikipedia.org/wiki/CSS) [![javascript](assets/javascript.png "javascript")](https://en.wikipedia.org/wiki/JavaScript) [![python](assets/python.png "python")](https://www.python.org/) [![nodejs](assets/nodejs.png "nodejs")](https://nodejs.org/en/) [![vuejs](assets/vuejs.png "vuejs")](https://vuejs.org/) [![sass](assets/sass.png "sass")](https://sass-lang.com/) [![typescript](assets/typescript.png "typescript")](https://www.typescriptlang.org/) [![postgresql](assets/postgres.png "postgresql")](https://www.postgresql.org/) [![docker](assets/docker.png "docker")](https://www.docker.com/) [![rust](assets/rust.png "rust")](https://www.rust-lang.org/)

## Special Skills

[![discord](assets/discord.png)](https://discord.com/)

- Develop discord bots in python(discordpy/pycord) and typescript(discordjs)

[![firefox](assets/firefox.png)](https://www.mozilla.org/en-US/firefox/new/)

- Develop Firefox extensions using manifest v2 in typescript

[![webpack](assets/webpack.png)](https://webpack.js.org/)

- Bundle Files using webpack

[![prisma](assets/prisma.png)](https://www.prisma.io/)

- Use prisma ORM to create and query databases.

## Github Stats

[![mohamedarish](https://streak-stats.demolab.com?user=mohamedarish&theme=tokyonight_duo&border_radius=5&date_format=j%20M%5B%20Y%5D&background=DD272700&currStreakNum=417E87&ring=025BDA&sideLabels=025BDA&sideNums=025BDA&dates=417E87&currStreakLabel=417E87&fire=417E87)](https://www.github.com/mohamedarish)

[![mohamedarish](https://github-readme-stats.vercel.app/api?username=mohamedarish&show_icons=true&theme=transparent)](https://www.github.com/mohamedarish)

[![language stats](https://github-readme-stats.vercel.app/api/top-langs/?username=mohamedarish&layout=compact&theme=transparent&langs_count=8)](https://github.com/mohamedarish)
